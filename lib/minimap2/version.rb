# frozen_string_literal: true

module Minimap2
  # Minimap2-2.25 (r1173)
  VERSION = "0.2.26.0"
end
